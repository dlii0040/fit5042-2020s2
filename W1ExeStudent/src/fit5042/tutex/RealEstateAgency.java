package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	try {
        propertyRepository.addProperty(new Property(81, "Caulfield Uni", 5, 300, 200));
        propertyRepository.addProperty(new Property(22, "Clayton Uni", 5, 300, 200));
        propertyRepository.addProperty(new Property(34, "Monash Uni", 5, 300, 200));
        propertyRepository.addProperty(new Property(41, "Melbourne Uni", 5, 300, 200));
        propertyRepository.addProperty(new Property(57, "Brisbane Uni", 5, 300, 200));
        System.out.println("5 properties added successfully");
    	}
    	catch(Exception e) {
    		System.out.print("Something wrong when add new property into repository");
    	}
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
    	int index = 1;
    	try {
			for (Property element:propertyRepository.getAllProperties()) {
				System.out.println(index + " " + element.getId() + " " + element.getAddress() + ", "
				+ element.getNumberOfBedrooms() + "BR(s) "
				+ element.getSize() + "sqm " + "$" + element.getPrice());
				index++;
			}
		} catch (Exception e) {
			System.out.print("Something wrong when get all properties");
		}
        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	Scanner sc = new Scanner(System.in);
        System.out.println("Eneter the ID of the property you want to search: ");
        try {
        	int id = sc.nextInt();
			Property targetProperty = propertyRepository.searchPropertyById(id);
			System.out.println(targetProperty.getId() + " " + targetProperty.getAddress() + ", "
					+ targetProperty.getNumberOfBedrooms() + "BR(s) "
					+ targetProperty.getSize() + "sqm " + "$" + targetProperty.getPrice());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
        	sc.close();
        }
        
    }
    
    public void run() {
    	System.out.println(this.name);
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
