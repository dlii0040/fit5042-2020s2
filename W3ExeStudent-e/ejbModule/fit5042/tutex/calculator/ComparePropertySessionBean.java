package fit5042.tutex.calculator;


import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	private Set<Property> list;
	
	public ComparePropertySessionBean() {
    	list = new HashSet<>();
    }
	@Override
	public void addProperty(Property property) {
		list.add(property);
	}
	
	@Override
	public void removeProperty(Property property) {
		for (Property element:list) {
			if (element.getPropertyId() == property.getPropertyId()) {
				list.remove(element);
				break;
			}
		}

	}
	
	@Override 
	public int bestPerRoom() {
		double average = 10000000.0;
		int bestPropertyId = 0;
		for (Property element: list) {
			if (average > element.getPrice()/element.getNumberOfBedrooms()) {
				average = element.getPrice()/element.getNumberOfBedrooms();
				bestPropertyId = element.getPropertyId();
			}
				
		}
		return bestPropertyId;
	}
	

}
